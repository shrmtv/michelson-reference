"""Structured representation of client output."""
import re

from typing import List


class InvalidClientOutput(Exception):
    """Raised when client output couldn't be parsed."""

    def __init__(self, client_output: str):
        super().__init__(self)
        self.client_output = client_output


class RunScriptResult:
    """Result of a 'get script' operation."""

    def __init__(self, client_output: str):
        # read storage output
        pattern_str = r"(?s)storage\n\s*(.*)\nemitted operations\n"
        match = re.search(pattern_str, client_output)
        if match is None:
            raise InvalidClientOutput(client_output)
        self.storage = match.groups()[0]

        # read operation output
        self.internal_operations = None
        pattern = r"(?s)emitted operations\n\s*(.*)\n  big_map diff"
        match = re.search(pattern, client_output)
        if match is not None:
            self.internal_operations = match.group(1)

        # read map diff output
        self.big_map_diff = []  # type: List
        pattern_str = r"big_map diff\n"
        match = re.search(pattern_str, client_output)
        if match is not None:
            compiled_pattern = re.compile(r"  ((New|Set|Del|Unset).*?)\n")
            for match_diff in compiled_pattern.finditer(client_output,
                                                        match.end(0)):
                self.big_map_diff.append([match_diff.group(1)])

        self.client_output = client_output
