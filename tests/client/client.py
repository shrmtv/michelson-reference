import os
import tempfile
import subprocess
import sys
from typing import Tuple, List, Optional

from tests.client import client_output


def format_command(cmd: List[str]) -> str:
    # TODO the displayed command may not be 'shell' ready, for instance
    # Michelson string parameters may requires additional quotes
    color_code = '\033[34m'
    endc = '\033[0m'
    cmd_str = " ".join(cmd)
    return f'{color_code}# {cmd_str}{endc}'


class Client:
    """A mockup Tezos client.

    Provides methods to call tezos-client returns structured
    representation of the client output.

    The most generic method to call the client is `run`. It calls the client
    with an arbitrary sequence of parameters and returns the stdout of the
    command. `CalledProcessError` is raised if command fails.

    Other commands such as `run_script`, `typecheck`... are wrapper
    over `run`, they set up the commands parameters and return a
    structured representation of the client output.
    """

    def __init__(self,
                 client_path: str,
                 protocol_hash: str,
                 base_dir: Optional[str] = None,
                 disable_disclaimer: bool = True):
        """
        Args:
            client_path (str): path to the client executable file
            base_dir (str): path to the client dir. If None, a temp file is
                            created.
            disable_disclaimer (bool): disable disclaimer
        Returns:
            A Client instance.
        """
        assert os.path.isfile(client_path), f"{client_path} is not a file"
        assert base_dir is None or os.path.isdir(base_dir), (f'{base_dir} not '
                                                             f'a dir')
        self._disable_disclaimer = disable_disclaimer

        if base_dir is None:
            base_dir = tempfile.mkdtemp(prefix='tezos-client.')
            assert base_dir
            self.base_dir = base_dir

        client = [client_path]
        client.extend(['-mode', 'mockup', '-protocol', protocol_hash])

        self._client = client

    def run(self,
            params: List[str],
            check: bool = True,
            trace: bool = False) -> str:
        """Like 'run_generic' but returns just stdout."""
        (stdout, _, _) = self.run_generic(params, check, trace)
        return stdout

    def run_generic(self,
                    params: List[str],
                    check: bool = True,
                    trace: bool = False) -> Tuple[str, str, int]:
        """Run an arbitrary command

        Args:
            params (list): list of parameters given to the tezos-client,
            check (bool): raises an exception if client call fails
            trace (bool): use '-l' option to trace RPCs
        Returns:
            (stdout of command, stderr of command, return code)

        The actual command will be displayed according to 'format_command'.
        Client output (stdout, stderr) will be displayed unprocessed.
        Fails with `CalledProcessError` if command fails
        """
        client = self._client
        base_dir_arg = ['-base-dir', self.base_dir]
        trace_opt = ['-l'] if trace else []
        cmd = client + base_dir_arg + trace_opt + params

        print(format_command(cmd))

        new_env = os.environ.copy()
        if self._disable_disclaimer:
            new_env["TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER"] = "Y"
            completed_process = subprocess.run(cmd,
                                               capture_output=True,
                                               text=True,
                                               check=False,
                                               env=new_env)
            stdout = completed_process.stdout
            stderr = completed_process.stderr
        if stdout:
            print(stdout)
        if stderr:
            print(stderr, file=sys.stderr)
        if check:
            completed_process.check_returncode()
            # `+ ""` makes pylint happy. It can't infer stdout/stderr can't
            # be `None` thanks to the `capture_output=True` option.
        return (stdout + "", stderr + "", completed_process.returncode)

    def typecheck(self, contract: str, file: bool = True) -> str:
        if file:
            assert os.path.isfile(contract), f'{contract} is not a file'
        return self.run(['typecheck', 'script', contract])

    def run_script(self,
                   contract: str,
                   storage: str,
                   inp: str,
                   amount: float = None,
                   trace_stack: bool = False,
                   params: List[str] = None,
                   file: bool = True) -> client_output.RunScriptResult:
        if file:
            assert os.path.isfile(contract), f'{contract} is not a file'
        params = [] if params is None else params
        cmd = ['run', 'script', contract, 'on', 'storage', storage, 'and',
               'input', inp] + params
        if amount is not None:
            cmd += ['-z', str(amount)]
        if trace_stack:
            cmd += ['--trace-stack']
        return client_output.RunScriptResult(self.run(cmd))
