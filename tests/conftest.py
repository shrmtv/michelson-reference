"""Hooks and fixtures.

A fixture defines code to be run before and after a (sequence of) test,
E.g. start and stop a server. The fixture is simply specified as a parameter
in the test function, and the yielded values is then accessible with this
parameter.
"""
import pytest

from tests.tools import paths
from tests.client.client import Client

@pytest.fixture(scope="class")
def client():
    """A client in mockup mode"""
    yield Client(paths.TEZOS_CLIENT, paths.TEZOS_PROTOCOL_HASH)
