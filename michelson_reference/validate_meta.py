import sys
import argparse

from typing import Generator, Dict, List, Any, Optional, IO

import yaml
from yaml.composer import Composer
from yaml.constructor import Constructor

import restructured_text


MICHELSON_META_RST_SCHEMA = {
    'instructions': {
        '*': {
            'documentation': True,
            'documentation_short': True,
            'examples': {
                '*': {
                    'name': True,
                    'description': True,
                    'input': True,
                    'initial_storage': True,
                    'final_storage': True,
                    'final_storage_pre': True,
                }
            }
        }
    },
    'types': {
        'documentation': True,
        'documentation_short': True
    }
}


def yaml_load_with_location(df: IO[Any]) -> Any:
    loader = yaml.Loader(df.read())
    def compose_node(parent, index):
        # the line number where the previous token has ended (plus empty lines)
        line = loader.line
        node = Composer.compose_node(loader, parent, index)
        node.__line__ = line + 1
        return node
    def construct_mapping(node, deep=False):
        mapping = Constructor.construct_mapping(loader, node, deep=deep)
        mapping['__line__'] = node.__line__
        return mapping
    loader.compose_node = compose_node # type: ignore
    loader.construct_mapping = construct_mapping # type: ignore
    return loader.get_single_data()

def eprint(msg: str) -> None:
    print(msg, file=sys.stderr)

def validate_rst_string(line: int,
                        key: str,
                        rst: str,
                        verbose: bool = False) -> None:
    (valid, opt_e) = restructured_text.validate(rst)
    if valid and verbose:
        eprint(f"{line}: {key} is valid rst!")
    if not valid:
        eprint(f"{line}: {key} is not valid rst. Message: {opt_e}")
        return False
    return True

def validate_rst_schema(
        key: List[str],
        rst_schema: Dict[str, Any],
        data: Dict[str, Any]):
    line = (data['__line__']
           if '__line__' in data
           else -1)
    valid = True

    for schema_key, schema_val in rst_schema.items():
        if schema_key != '*' and not schema_key in data:
            continue

        if schema_key == '*':
            if isinstance(data, list):
                for data_key, data_val in enumerate(data):
                    valid = valid and \
                        validate_rst_schema(key + [str(data_key)], schema_val, data_val)
            elif isinstance(data, dict):
                for data_key, data_val in data.items():
                    if data_key == '__line__':
                        continue
                    valid = valid and \
                        validate_rst_schema(key + [data_key], schema_val, data_val)

        elif isinstance(schema_val, dict):
            valid = valid and \
                validate_rst_schema(key + [schema_key], schema_val, data[schema_key])

        elif schema_val:
            valid = valid and \
                validate_rst_string(
                    line,
                    '.'.join(key + [schema_key]),
                    data[schema_key]
                )

    return valid

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="""
Validate restructured text fields in a Michelson meta data file.
        """)
    parser.add_argument('data_file',
                        metavar='data.yaml',
                        type=argparse.FileType('r'),
                        help='the data file')
    args = parser.parse_args()

    with args.data_file as df:
        data = yaml_load_with_location(df)
        valid = validate_rst_schema([], MICHELSON_META_RST_SCHEMA, data)
        if valid:
            eprint(f"No problems found in {df.name}.")
            code = 0
        else:
            code = 1
        sys.exit(code)


