from docutils.core import publish_parts
from docutils.utils import SystemMessage

import io

def publish_parts_exn(**kwargs):
    """As publish_parts from docutils.core, but with strict error settings.

    Ensures that all warnings results in errors with a traceback,
    which simplifies troublesohoting.
    """

    # Use a io.StringIO as the warning stream to prevent warnings from
    # being printed to sys.stderr.
    stream = io.StringIO()

    overrides = {'warning_stream': stream,
                 'halt_level': 2,  # warnings and up; adjust as necessary
                 'traceback': True}
    return publish_parts(**kwargs, settings_overrides=overrides)

def validate(source: str):
    try:
        publish_parts_exn(source=source, writer_name='html')
        return (True, None)
    except SystemMessage as e:
        return (False, e)
