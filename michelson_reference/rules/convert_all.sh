#!/bin/bash

for i in $(ls *.tex); do
	echo $i
	i=${i/.tex/}
	pdflatex -interaction=batchmode $i
	pdftoppm -r 300 -png $i.pdf > $i.png
done

mkdir -p ../docs/images/rules/
cp -v *.png ../docs/images/rules/
